﻿using DAL.Entities;
using DAL.EntitiesConfigurations;
using Microsoft.EntityFrameworkCore;
using System;

namespace DAL
{
    public class EfContext : DbContext
    {
        public EfContext(DbContextOptions<EfContext> options)
            : base(options)
        {
        }

        public DbSet<Upload> Uploads { get; set; }
        public DbSet<Entities.Login> Logins { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ArticlesClassificators> ArticlesClassificators { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Classificators> Classificators { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UploadEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
            modelBuilder.ApplyConfiguration(new ArticleCLassificatorEntitiesConfiguration());
        }
    }
}