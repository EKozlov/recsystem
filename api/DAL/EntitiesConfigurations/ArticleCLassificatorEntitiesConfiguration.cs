﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    internal class ArticleCLassificatorEntitiesConfiguration : IEntityTypeConfiguration<ArticlesClassificators>
    {
        public void Configure(EntityTypeBuilder<ArticlesClassificators> builder)
        {
            builder.HasKey(x => new { x.ArticleId, x.ClassificatorId });

            builder.ToTable("ArticlesClassificators");

            builder.HasOne<Article>()
                .WithMany()
                .HasForeignKey(x => x.ArticleId).IsRequired();

            builder.HasOne<Classificators>()
                .WithMany()
                .HasForeignKey(x => x.ClassificatorId).IsRequired();

        }
    }
}
