﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.API.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
