﻿using ClassifierGen;
using DAL;
using Interfaces;
using Interfaces.Contexts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace BL.Commands.CreateArticle
{
    public class CreateArticleCommand : ICommand<ArticleCreateContext>
    {

        private readonly EfContext _db;
        public CreateArticleCommand( EfContext db)
        {

            _db = db;
        }
        public static string BaseResultPath = Path.GetFullPath(@"..\Documents");
        public async  Task<CommandResult> ExecuteAsync(ArticleCreateContext context)
        {
              

            var regex = new Regex(@"[\p{IsCyrillic}]+").ToString();

            foreach (var f in Directory.EnumerateFiles(BaseResultPath, "*.docx"))
            {
                

                using (var document = DocX.Load(f))
                {
                    if (document.FindUniqueByPattern(regex, RegexOptions.IgnoreCase).Count > 0)
                    {
                        var classificators = new GenClassifier().Generate(document);
                      
                    }
                }

            }

            throw new NotImplementedException();
        }






    }
}
