﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ClassifierGen.Constants
{
    public static class Constants
    {
        public static string keywordsPath = Path.GetFullPath(@"..\ClassifierGen\KeyWords\Keywords.txt");

        public static string BaseResultPath = Path.GetFullPath(@"..\GenClassifier\mystem\mystem.exe");

        public static double tfMinValue = 10;

        public static int numberMatches = 4;
    }
}
