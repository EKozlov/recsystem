﻿using DAL.Entities;
using Libs.TermFrequencyParamsCalculation.Commands;
using RecSystem.Libs.Lemmatizer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Xceed.Document.NET;
using Xceed.Words.NET;


namespace ClassifierGen
{
    public class GenClassifier
    {
        List<KeyWords> keyWords = new List<KeyWords>();

        List<ClassificatorsTheme> classificatorsThemes = new List<ClassificatorsTheme>();

        public List<ClassificatorsTheme> Generate(DocX document)
        {

            var normalizeTupleText = GetNormalizeTokens(document);

            var wordsCount = normalizeTupleText.Item2.Split(' ').Length;

            var articleKeyWords = getKeyWords(normalizeTupleText.Item2, wordsCount);

            articleKeyWords.Add(new KeyWords { Word = normalizeTupleText.Item1 });

            int count = 0;

            using (StreamReader reader = new StreamReader(Constants.Constants.keywordsPath))
            {
                var keywordsTheme = reader.ReadToEnd().Split('!');

                for (int i = 1; i < keywordsTheme.Length; i++)
                {
                    var keywords = keywordsTheme[i].Split(':')[1].Split(',');
                    foreach (var item in articleKeyWords)
                    {
                        if (keywords.Contains(item.Word))
                            count++;
                    }
                    classificatorsThemes.Add(new ClassificatorsTheme
                    {
                        Count = count,
                        Name = keywordsTheme[i].Split(':')[0],
                        Probability = ((articleKeyWords.Count() % 100) * count).ToString() + "%"
                    });
                    count = 0;
                }
            }
            return classificatorsThemes.Where(x => x.Count >= Constants.Constants.numberMatches).ToList();
        }

        public Tuple<string, string> GetNormalizeTokens(DocX document)
        {
            var paragraph = document.Paragraphs[0].Text;

            var text = document.Xml.Value;

            var tokenizedText = string.Join(" ", text);

            var tokenizedParagraph = string.Join(" ", paragraph);

            var currentDirectory = AppDomain.CurrentDomain.BaseDirectory + "mystem.exe";

            var paragraphText = new Lemmatizer(currentDirectory).LemmatizeText(tokenizedParagraph)
                                 .Trim()
                                 .Replace("   ", " ");

            var lemmasText = new Lemmatizer(currentDirectory).LemmatizeText(tokenizedText)
                                  .Trim()
                                  .Replace("   ", " ");

            return Tuple.Create(paragraphText, lemmasText);
        }

        public List<KeyWords> getKeyWords(string text, int wordsCount)
        {
            var tfCalcResult = new TfCalculateCommand(text).Handle().ToArray();
            var tfTableData = tfCalcResult
                .Select(tfp => new object[] {
                    tfp.Term,
                    tfp.Value,
                    tfp.DocumentName
                });

            foreach (var item in tfTableData)
            {
                var kekeke = Convert.ToDouble(item.GetValue(1));
                if (Convert.ToDouble(item.GetValue(1)) * wordsCount > Constants.Constants.tfMinValue && item.GetValue(0).ToString().Length > 3)
                {
                    keyWords.Add(new KeyWords
                    {
                        Word = item.GetValue(0).ToString(),
                        Value = Convert.ToDouble(item.GetValue(1))
                    });
                }

            }
            return keyWords;
        }
    }
}
