﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using Interfaces.Contexts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.API.Models;

namespace Web.API.Controllers
{
    [Route("api/article")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly ICommandHandler _commandHandler;

        public ArticleController(ICommandHandler commandHandler)
        {
            _commandHandler = commandHandler;
        }

        [HttpPost("")]
        [ProducesResponseType(typeof(ArticleModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResultCode), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] ArticleModel model, int time_offset = 0)
        {
            

            var commandResult = await _commandHandler.Execute(new ArticleCreateContext
            {
                ArticleText = "ddddddd"
            });


            return Ok();

        }




    }
}