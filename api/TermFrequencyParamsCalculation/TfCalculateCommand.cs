﻿using Libs.TermFrequencyParamsCalculation.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Libs.TermFrequencyParamsCalculation.Commands
{
    public class TfCalculateCommand
    {
        private readonly string _documentText;

        public TfCalculateCommand(string documentText)
        {
            _documentText = documentText;
        }

        public IEnumerable<DocumentBasedFrequencyCalculationResult> Handle()
        {
            var result = new List<DocumentBasedFrequencyCalculationResult>();

            var fileText = _documentText;
            var wordsCount = fileText.Split(' ').Length;
            var tfParamsList = fileText
                            .Split(' ')
                            .Where(w => !string.IsNullOrEmpty(w))
                            .GroupBy(w => w)
                            .Select(g => new DocumentBasedFrequencyCalculationResult(
                                g.Key,
                                 Math.Round(g.Count() / (double)wordsCount, 5),
                                fileText.Replace(_documentText + "\\", "")));
            result.AddRange(tfParamsList);
            return result.OrderByDescending(tfp => tfp.Value)
                            .ThenBy(tfp => tfp.Term);
        }
    }
}