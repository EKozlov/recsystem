﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class KeyWords
    {
        public string Word { get; set; }
        public double Value { get; set; }
    }
}
