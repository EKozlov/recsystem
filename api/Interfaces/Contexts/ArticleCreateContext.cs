﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public class ArticleCreateContext : IContext
    {
        public string ArticleText { get; set; }
    
    }
}
